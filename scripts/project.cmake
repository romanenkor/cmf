cmake_minimum_required(VERSION 3.12.0)

function(build)
    set(options LIBRARY EXECUTABLE)
    set(oneValueArgs CXX)
    set(multiValueArgs NAME INCLUDE SOURCE TEST DEPENDS)
    cmake_parse_arguments(L "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGV})

    if(L_EXECUTABLE)
        add_executable(${L_NAME})
    elseif(L_LIBRARY)
        add_library(${L_NAME})
    endif()

    list(LENGTH L_NAME L_SIZE)
    if(L_SIZE LESS 2)
        set(TYPE PRIVATE)
    else()
        list(GET L_NAME 1 TYPE)
    endif()
    list(GET L_NAME 0 L_NAME)

    if(L_CXX)
        target_compile_features(${L_NAME} ${TYPE} cxx_std_${L_CXX})
    endif()

    if(L_INCLUDE)
        target_include_directories(${L_NAME} ${L_INCLUDE})
    endif()

    ftl_default_options(${L_NAME} ${TYPE} COMPILE LINK DEFINITIONS)

    set(sources)
    foreach(pattern ${L_SOURCE})
        if(pattern MATCHES "PRIVATE|PUBLIC|INTERFACE")
            list(APPEND sources ${pattern})
        else()
            file(GLOB_RECURSE files ${pattern})
            list(APPEND sources ${files})
        endif()
    endforeach()

    if(L_SOURCE)
        target_sources(${L_NAME} ${sources})
    endif()

    if(L_DEPENDS)
        target_link_libraries(${L_NAME} ${L_DEPENDS})
    endif()

    if(L_TEST)
        ftl_add_tests(${L_TEST})
    endif()
endfunction()

function(ftl_add_tests)
    include(CTest)
    if(NOT PROJECT_BINARY_DIR OR NOT BUILD_TESTING OR NOT CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
        return()
    endif()
    set(sources)
    foreach(pattern ${ARGV})
        file(GLOB files ${pattern})
        list(APPEND sources ${files})
    endforeach()
    foreach(source ${sources})
        string(REGEX MATCH  "([^\\/]*)\\..*$" tmp ${source})
        set(module ${CMAKE_MATCH_1})
        if(module STREQUAL "test" AND CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
            continue()
        endif()

        write(${PROJECT_BINARY_DIR}/tests/${module}/main.cpp
            "#define ENABLE_UNIT_TESTS"
            "#include <ftl/test.hpp>"
            "#include \"${source}\""
        )

        write(${PROJECT_BINARY_DIR}/tests/${module}/redundant.cpp
            "#define REDUNDANT_UNIT_TESTS"
            "#include \"${source}\""
        )

        add_executable(${module}
            ${PROJECT_BINARY_DIR}/tests/${module}/main.cpp
            ${PROJECT_BINARY_DIR}/tests/${module}/redundant.cpp
        )

        set_target_properties(${module}
            PROPERTIES
                RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/tests/${module}
        )

        ftl_default_options(${module} ${TYPE} COMPILE LINK DEFINITIONS)

        if(TYPE STREQUAL "INTERFACE")
            target_link_libraries(${module} ${L_NAME})
        endif()
        
        add_test(NAME ${module} COMMAND ${module})
    endforeach()
endfunction()
