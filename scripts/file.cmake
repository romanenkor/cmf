cmake_minimum_required(VERSION 3.12.0)

function(write file)
    file(WRITE "${file}" "")
    foreach(line ${ARGN})
        file(APPEND "${file}" "${line}\n")
    endforeach()
endfunction()
