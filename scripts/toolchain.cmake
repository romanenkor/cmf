cmake_minimum_required(VERSION 3.12.0)

set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

function(ftl_parse_flags var)
    string(CONCAT pattern
        "COMMON|Absoft|ADSP|AppleClang|ARMCC|Bruce|Borland|CCur|Clang|Cray|"
        "Embarcadero|G95|GNU|HP|IAR|Intel|MIPSpro|MSVC|NVIDIA|OpenWatcom|"
        "PGI|Flang|PathScale|SDCC|SunPro|TI|TinyCC|XL|VisualAge|zOS"
    )
    unset(output)
    foreach(arg ${ARGN})
        unset(match)
        if(arg MATCHES ${pattern})
            set(match ${CMAKE_MATCH_0})
            continue()
        endif()
        if (match STREQUAL "COMMON")
            list(APPEND output "${arg}")
        else()
            list(APPEND output "$<$<C_COMPILER_ID:${match}>:${arg}>")
            list(APPEND output "$<$<CXX_COMPILER_ID:${match}>:${arg}>")
        endif()
    endforeach()
    set(${var} ${output} PARENT_SCOPE)
endfunction()

function(ftl_compile_options target type)
    ftl_parse_flags(FLAGS ${ARGN})
    target_compile_options(${target} ${type} ${FLAGS})
endfunction()

function(ftl_link_options target)
    ftl_parse_flags(FLAGS ${ARGN})
    target_link_options(${target} ${type} ${FLAGS})
endfunction()

function(ftl_compile_definitions target)
    ftl_parse_flags(FLAGS ${ARGN})
    target_compile_definitions(${target} ${type} ${FLAGS})
endfunction()

function(ftl_default_options target type)
    set(options COMPILE LINK DEFINITIONS)
    set(single_value_args)
    set(multi_value_args)
    cmake_parse_arguments(OPT "${options}" "${single_value_args}" "${multi_value_args}" ${ARGN})

    if(OPT_COMPILE)
        ftl_compile_options(${target} ${type}
            GNU|Clang
                -Werror -Wall -Wextra -Wshadow -Wcast-align -Wcast-qual 
                -Wdisabled-optimization -Wdiv-by-zero -Wendif-labels 
                -Wformat-extra-args -Wformat-nonliteral -Wformat-security 
                -Wformat-y2k -Wimport -Winit-self -Winvalid-pch 
                -Werror=missing-braces -Wmissing-declarations 
                -Wno-missing-format-attribute -Wmissing-include-dirs
                -Wpacked -Wpointer-arith -Wreturn-type -Wsequence-point 
                -Wsign-compare -Wstrict-aliasing -Wstrict-aliasing=2 -Wswitch 
                -Wswitch-default -Werror=undef -Wno-unused -Wvariadic-macros 
                -Wwrite-strings -Werror=declaration-after-statement 
                -Werror=implicit-function-declaration -Werror=nested-externs 
                -Werror=old-style-definition -Werror=strict-prototypes
                -Wnon-virtual-dtor -Wcast-align -Woverloaded-virtual
                -Wconversion -Wformat=2 -Wpedantic -Wsign-conversion -Wpedantic
                -Wsign-conversion -Wdouble-promotion -Wmultichar 
            GNU
                -Wmisleading-indentation -Wduplicated-cond -Wuseless-cast
                -Wduplicated-branches -Wlogical-op -Wnull-dereference
            MSVC
                /Wx /permissive /W4 /w14640 /w14242 /w14254 /w14263 /w14265
                /w14287 /w14296 /w14311 /w14545 /w14546 /w14547 /w14549
                /w14555 /w14619 /w14640 /w14826 /w14905 /w14906 /w14928
        )
    endif()

    if(OPT_LINK)
    endif()

    if(OPT_DEFINITIONS AND CMAKE_BUILD_TYPE STREQUAL "Release")
        ftl_compile_definitions(${target} ${type} RELEASE NDEBUG)
    else()
        ftl_compile_definitions(${target} ${type} DEBUG)
    endif()
endfunction()
