cmake_minimum_required(VERSION 3.12.0)

file(GLOB scripts ${CMAKE_CURRENT_LIST_DIR}/scripts/*)
foreach(script ${scripts})
    include(${script})
endforeach()
